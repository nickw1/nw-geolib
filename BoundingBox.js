
class BoundingBox {

    constructor(w, s, e, n) {
        this.bottomLeft= {};
        this.topRight = {};
        this.bottomLeft.x = w;
        this.bottomLeft.y = s;
        this.topRight.x = e;
        this.topRight.y = n;
    }

    contains(p) {
        try {
        return p[0] > this.bottomLeft.x && p[0] < this.topRight.x && p[1] > this.bottomLeft.y && p[1] < this.topRight.y;
        } catch(e) { console.log(e); }
    }
    
    toString() {
        return `${this.bottomLeft.x} ${this.bottomLeft.y} ${this.topRight.x} ${this.topRight.y}`;
    }
}

module.exports = BoundingBox;

